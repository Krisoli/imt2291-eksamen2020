<?php
// Denne filen henter data fra formen i index.html og legger dem inn i databasen.
// Først kobles filen til twig, så sjekker filen om alle feltene i formen er fylt inn
// i henholdt til kriteriene som er listet i if/else vist under. 
// Hvis alle kriteriene er oppfylt legges brukeren inn i databasen med data
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));

$res['status'] = '';
if(isset($_POST['submit'])&&!empty($_POST['uname'])&&!empty($_POST['pwd'])&&!empty($_POST['firstName'])&&!empty($_POST['lastName'])) {
    require_once 'classes/DB.php';
    $db = DB::getDBConnection();

    $email = $_POST["uname"];
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $res['status'] = 'Fyll inn email i riktig format';
    } else if(strlen($_POST['pwd']) < 8) {
        $res['status'] = 'Passord må være minimum 8 karakterer';
    } else if(strlen($_POST['firstName']) < 0) {
        $res['status'] = 'Fornavn må være minimum tre karakterer';
    } else if(strlen($_POST['lastName']) < 0) {
        $res['status'] = 'Etternavn må være minimum tre karakterer';
    } else {
        $passwordHash = password_hash($_POST['pwd'], PASSWORD_BCRYPT, array("cost" => 12));

        $sql = "INSERT INTO user (uname, pwd, firstName, lastName) VALUES (:uname, :pwd, :firstName, :lastName)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":uname", $_POST['uname']);
        $stmt->bindParam(":pwd", $passwordHash);
        $stmt->bindParam(":firstName", $_POST['firstName']);
        $stmt->bindParam(":lastName", $_POST['lastName']);

        $result = $stmt->execute();
        if($result) {
            $res['status'] = $_POST['firstName'] . " " . $_POST['lastName'] . " er lagt til i databasen";
        } else {
            $res['status'] = 'Brukeren ble ikke lagt til i databasen';
        }
    }
} 
echo $twig->render('index.html', $res); // Denne kodelinjen gjør at addUser.php henter innholdet på index.html siden. 
                                        // $res blir sendt med til index.html filen slik at den kan hentes med twig

