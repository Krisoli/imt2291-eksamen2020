import { LitElement, html, css } from "../../node_modules/lit-element/lit-element.js";

class EditUser extends LitElement {
  static get properties() {
    return {
      user: { type: Object }
    };
  }
  static get styles() {
    return css`
        label {
          display: inline-block;
          width: 9em;
        }
        input {
            display: inline-block;
            width: 12em;
        }
    `;
  }
  // Formen fra oppgave 2 
  // Siden user er deklarert øverst i denne filen kan vi hent den med "this.user" som er vist i input feltene under
  // Slik blir valuen til inputfeltene lik valuen til den brukeren vi trykker på
  render() {
    return html`
    <div class="frm">
        <form>
            <label>Brukernavn</label><input type="email" name="uname" id="uname" value="${this.user.uname}"> <br>
            <label>Gammelt passord</label><input type="password" name="oldpwd" id="oldpwd"> <br>
            <label>Nytt passord</label><input type="password" name="pwd" id="pwd"> <br>
            <label>Fornavn</label><input type="text" name="firstName" id="firstName" value="${this.user.firstName}"> <br>
            <label>Etternavn</label><input type="text" name="lastName" id="lastName" value="${this.user.lastName}"> <br>
            <br><input type="submit" name="submit" id="submit" value="Oppdater bruker" @click="${this.edit}"> <br>
          </form>
          <h3 class="response"></h3>
    </div>
    `;
  }
  // Denne funksjonen sender den oppdaterte infromasjonen om brukeren til databasen.
  // Data inneholder alle verdiene som formen skal sende til databasen
  edit(e) {
    const data = new FormData(e.target.form);
    window.uid = this.user.uid;
    data.append('uid', window.uid);
    fetch(`http://localhost/eksamen2020/oppgave1-3/www/api/updateUser.php`, {
          method: "POST",
          credentials: "include",
          body: data
      }).then(res => {
          return res.json();
      }).then(res => {
          console.log(res.status);
      })
    }
}
customElements.define('edit-user', EditUser);
