// Denne funksjonen henter brukere fra databasen
// Koden er inspirert fra tidligere eksamner som ligger på blackboard
window.uid = -1;
function editUser(uid) {
    fetch(`api/fetchUser.php?id=${uid}`)
    .then(res=>res.json())
    .then(user=>{
        window.uid = user.uid;
        // Brukerens data blir lagt inn i formen. 
        document.getElementById('uname').value = user.uname;
        document.getElementById('firstName').value = user.firstName;
        document.getElementById('lastName').value = user.lastName;
    })

}
// Legger til et event på submit knappen og sender den oppdaterte formen med brukerdata til databasen
document.querySelector('#submit').addEventListener('click', e=>{
    e.preventDefault();
    if(window.uid> -1) {
        // Data innholder verdiene i formen som skal sendes til databasen
        const data = new FormData(e.target.form);
        data.append('uid', window.uid);
        fetch('api/updateUser.php', {
            method: 'POST',
            body: data
        }).then(res=>res.json())
        .then(res=>{
            const response = document.querySelector('.response');
            if (res.status =='success') {
            response.innerHTML = "Brukerinformasjonen er oppdatert";
         } else {
            response.innerHTML = "Kunne ikke oppdatere brukerinformasjonen";
          }
        })
    }
 })